package repo;

import model.IndividualTestAnswer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public interface IndividualTestAnswerRepo extends JpaRepository<IndividualTestAnswer, Long> {
    Optional<IndividualTestAnswer> findById(Long id);
}
