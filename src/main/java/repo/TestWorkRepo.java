package repo;

import model.Test;
import model.TestWork;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TestWorkRepo extends JpaRepository<TestWork, Long> {
    TestWork getById(Long id);
    TestWork findByCode(String code);
    List<TestWork> findAllByOwnerAndOver (Long id, boolean isOver);
    List<TestWork> findAllByOwner(Long id);
}
