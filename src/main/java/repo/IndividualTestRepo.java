package repo;

import model.IndividualTest;
import model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface IndividualTestRepo extends JpaRepository<IndividualTest, Long> {
    IndividualTest getByTestWork(Long id);

    List<IndividualTest> getAllByOwner(Long id);

    List<IndividualTest> getAllByOwner(User owner);
    List<IndividualTest> getAllByOwnerAndFinished(Long Id, Boolean finished);

}
