package repo;

import model.Task;
import model.UserGoogle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserGoogleRepo extends JpaRepository<UserGoogle, String> {
    UserGoogle getById(String id);
}
