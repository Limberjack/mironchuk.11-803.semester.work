package repo;

import model.Test;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TestRepo extends JpaRepository<Test, Long> {
    Test getById(Long id);
    List<Test> getAllByOwner(Long id);
}
