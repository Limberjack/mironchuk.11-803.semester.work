package repo;

import model.IndividualTestTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IndividualTestTaskRepo extends JpaRepository<IndividualTestTask, Long> {
    List<IndividualTestTask> getAllByIndividualTest(Long id);

}
