package repo;

import model.Task;
import model.Test;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface TaskRepo extends JpaRepository<Task, Long> {
    List<Task> findAllByTest(Long id);
}
