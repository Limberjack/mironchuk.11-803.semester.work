package model;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name="testwork_")
public class TestWork {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long begin;

    private Long end;

    private Long duration;

    private String name;

    private Integer numberOfQuestions;

    private Integer ansPerQuest;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="test_id")
    private Test test;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="user_id")
    private User owner;

    private String code;

    private Boolean over;

    public Boolean isOver() {
        return over;
    }

    public void setOver(Boolean over) {
        this.over = over;
    }

    public void generateCode(){
        code = UUID.randomUUID().toString();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBegin() {
        return begin;
    }

    public void setBegin(Long begin) {
        this.begin = begin;
    }

    public Long getEnd() {
        return end;
    }

    public void setEnd(Long end) {
        this.end = end;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumberOfQuestions() {
        return numberOfQuestions;
    }

    public void setNumberOfQuestions(Integer numberOfQuestions) {
        this.numberOfQuestions = numberOfQuestions;
    }

    public Integer getAnsPerQuest() {
        return ansPerQuest;
    }

    public void setAnsPerQuest(Integer ansPerQuest) {
        this.ansPerQuest = ansPerQuest;
    }

    public Boolean getOver() {
        return over;
    }
}
