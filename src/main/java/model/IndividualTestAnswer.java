package model;

import javax.persistence.*;

@Entity
@Table(name = "ind_test_ans")
public class IndividualTestAnswer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne(fetch =FetchType.EAGER)
    @JoinColumn(name = "ind_ans_ans")
    private Answer answer;

    @ManyToOne(fetch =FetchType.LAZY)
    @JoinColumn(name = "ind_ans_ind_task")
    private IndividualTestTask individualTestTask;

    private Boolean isSelected;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }

    public IndividualTestTask getIndividualTestTask() {
        return individualTestTask;
    }

    public void setIndividualTestTask(IndividualTestTask individualTestTask) {
        this.individualTestTask = individualTestTask;
    }

    public Boolean getSelected() {
        return isSelected;
    }

    public void setSelected(Boolean selected) {
        isSelected = selected;
    }

}
