package model;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name="individual_task")
public class IndividualTestTask {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "individual_test_task")
    private IndividualTest individualTest;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "individual_task_task")
    private Task task;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name="ind_task_ind_ans")
    List<IndividualTestAnswer> answers;

    private Integer score;

    public List<IndividualTestAnswer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<IndividualTestAnswer> answers) {
        this.answers = answers;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public IndividualTest getIndividualTest() {
        return individualTest;
    }

    public void setIndividualTest(IndividualTest individualTest) {
        this.individualTest = individualTest;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }
}
