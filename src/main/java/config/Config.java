package config;

import model.User;
import model.UserGoogle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import service.UserService;

import java.security.Principal;

@EnableOAuth2Sso
@Configuration
@EnableWebSecurity
@ComponentScan(basePackages ={"repo", "con", "service"} )
public class Config extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserService userService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.httpBasic().disable();
        /*http.
                authorizeRequests()
                .mvcMatchers("/login")
                .permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .csrf().disable();*/

        http.authorizeRequests()
                .antMatchers("/signup", "/signin")
                .permitAll()
                .anyRequest()
                .authenticated()
        .and().csrf().disable();
    }
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        super.configure(auth);
        auth.userDetailsService(userService).passwordEncoder(passwordEncoder);
    }

    @Bean
    public PrincipalExtractor principalExtractor(UserService userService){
        return map -> {
            String id = (String) map.get("sub");
            UserGoogle user = userService.getGoogleUser(id);
            if(user == null){
                user = new UserGoogle();
                user.getUser().setEmail((String) map.get("email"));
                user.getUser().setName((String) map.get("name"));
            }
            userService.saveGoogleUser(user);
            return user;
        };
    }

    @Bean
    public PasswordEncoder getPasswordEncoder(){
        return new BCryptPasswordEncoder(8);
    }
}