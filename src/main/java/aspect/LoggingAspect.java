package aspect;


import model.IndividualTest;
import model.Test;
import model.TestWork;
import model.User;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {
    @Autowired
    private Logger logger;

   @After(value = "execution(* service.TestWorkService.addTestWork(..))")
    public void after(JoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();
        TestWork testWork = (TestWork) args[0];
        logger.log("User " + testWork.getOwner().getId() + " added testwork");
    }

    @Before(value = "execution(* service.IndividualTestService.finishTheTest(..))")
    public void before(JoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();
        IndividualTest test = (IndividualTest) args[0];
        logger.log("User " + test.getOwner().getId() + " finished the test " + test.getTestWork().getTest().getName());
    }
}