package aspect;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.ApplicationScope;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@Component
public class Logger {
    @Autowired
    @Qualifier("logFileWriter")
    FileWriter fileWriter;

    @SneakyThrows
    public void log(String s) {
        try {
            fileWriter.append(s + "\n");
            fileWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Bean(value = "logFileWriter")
    @ApplicationScope
    public FileWriter fileWriter() {
        try {
            File file = new File("log.txt");
            if (!file.exists()) {
                file.createNewFile();
            }
            return new FileWriter(file, true);

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}