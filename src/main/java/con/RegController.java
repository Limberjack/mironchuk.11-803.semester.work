package con;

import con.support.Validator;
import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import service.UserService;

import java.util.Map;


@Controller
public class RegController {
    @Autowired
    private UserService userService;

    @GetMapping("/signup")
    public String signup(){
        return "registration";
    }

    @GetMapping("/signup/error")
    public ModelAndView signInError(){
        ModelAndView modelAndView = new ModelAndView("registration");
        modelAndView.addObject("error", "invalid password or email");
        return modelAndView;
    }

    @PostMapping("/signup")
    public String reg(@RequestParam String email, @RequestParam String password, @RequestParam String name, Map<String, Object> model){
        if(!Validator.isEmailValid(email) || !Validator.isPasswordValid(password))
            return "redirect:/signup/error";

        if(userService.loadUserByUsername(email)!=null){
            model.put("takenEmailError", null);
            return "registration";
        }

        User user = new User();
        user.setName(name);
        user.setPassword(password);
        user.setEmail(email);
        userService.addUser(user);

        return "redirect:/signin";
    }

  /*  @GetMapping("/activate/{code}")
    public String activate(Model model, @PathVariable String code) {
        boolean isActivated = userService.activateUser(code);

        if (isActivated) {
            model.addAttribute("message", "User successfully activated");
        } else {
            model.addAttribute("message", "Activation code is not found!");
        }

        return "login";
    }*/

    @Bean
    public UserService getUserService(){
        return new UserService();
    }
}
