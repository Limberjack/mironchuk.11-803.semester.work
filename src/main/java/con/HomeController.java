package con;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import service.TestService;
import service.TestWorkService;
import session.UserSession;

import javax.servlet.http.HttpSession;

@Controller
public class HomeController {
    @Autowired
    TestService testService;
    @Autowired
    TestWorkService testWorkService;

    @GetMapping("/home")
    public ModelAndView home(HttpSession session){
        ModelAndView modelAndView = new ModelAndView("home");

        modelAndView.addObject("userSession", (UserSession)session.getAttribute("userSession"));
        modelAndView.addObject("tests", testService.getTests(((UserSession) session.getAttribute("userSession")).getUser()));
        modelAndView.addObject("testWorks", testWorkService.getActive(((UserSession) session.getAttribute("userSession")).getUser()));

        return modelAndView;
    }
}
