package con;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class EmptyPageController {
    @GetMapping("/")
    public String redirect(){
        return "redirect:/signin";
    }
}
