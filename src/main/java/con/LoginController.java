package con;

import con.support.Validator;
import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import service.UserService;
import session.UserSession;

import javax.servlet.http.HttpSession;
import java.util.Map;

@ComponentScan(basePackages ={"repo", "config", "service"} )
@Controller
public class LoginController {

    @Autowired
    private UserService userService;

    @GetMapping("/signin")
    public String greeting() {
        return "login";
    }

    @GetMapping("/signin/error")
    public ModelAndView signInError(){
        ModelAndView modelAndView = new ModelAndView("login");
        modelAndView.addObject("error", "invalid password or email");
        return modelAndView;
    }

    @PostMapping("/signin")
    public String signin(@RequestParam String email, @RequestParam String password, Map <String, Object> model, HttpSession httpSession){
        if(!Validator.isEmailValid(email) || !Validator.isPasswordValid(password))
            return "redirect /signin/error";
        User user = userService.getUser(email, password);
        if(user!=null){
            httpSession.setMaxInactiveInterval(3600);
            httpSession.setAttribute("userSession", new UserSession(user));
            return "redirect:/home";
        }
        model.put("error", "");
        return "login/error";
    }


}
