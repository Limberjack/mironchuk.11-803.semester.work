package con.support;

public class Validator {
    public static boolean isEmailValid(String email){
        if(!email.contains("@") && !email.contains("."))
            return false;
        return true;
    }

    public static boolean isPasswordValid(String password){
       return   password.length() > 8
                && upRegisterCheck(password)
                && bottomRegisterCheck(password)
                && freeOutOfTrash(password);

    }

    private static boolean upRegisterCheck(String string){
        char password[] = string.toCharArray();
        for (int i = 0; i < password.length; i++) {
            if(password[i] <= 'Z' && password[i] >= 'A')
                return true;
        }
        return false;
    }
    private static boolean bottomRegisterCheck(String string){
        char password[] = string.toCharArray();
        for (int i = 0; i < password.length; i++) {
            if(password[i] <= 'z' && password[i] >= 'a')
                return true;
        }
        return false;
    }

    private static boolean freeOutOfTrash(String string){
        char password[] = string.toCharArray();
        for (int i = 0; i < password.length; i++) {
            if(
                    !(password[i] <= 'z' && password[i] >= 'a') &&
                    !(password[i] <= 'Z' && password[i] >= 'A') &&
                    !(password[i] <= '9' && password[i] >= '0')
            )
                return false;
        }
        return true;
    }
}
