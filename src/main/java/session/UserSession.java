package session;

import model.IndividualTest;
import model.IndividualTestTask;
import model.User;
import org.springframework.web.context.annotation.SessionScope;

@SessionScope
public class UserSession {
    public UserSession(User user) {
        this.user = user;
    }

    public UserSession() {
    }

    private User user;
    private IndividualTest individualTest;

    public IndividualTest getIndividualTest() {
        return individualTest;
    }

    public void setIndividualTest(IndividualTest individualTest) {
        this.individualTest = individualTest;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
