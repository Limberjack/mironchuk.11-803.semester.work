package service;

import model.TestWork;
import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repo.TestWorkRepo;

import javax.persistence.ManyToOne;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Service
public class TestWorkService {

    @Autowired
    private TestWorkRepo testWorkRepo;

    public void addTestWork(TestWork testWork){
        testWork.generateCode();
        if(testWork.getBegin() == null)
            testWork.setBegin((long) System.currentTimeMillis());
        if(testWork.getEnd() == null || testWork.getEnd() == 0)
            testWork.setEnd((long) -1);
        if(testWork.getDuration() == null || testWork.getDuration() == 0)
            testWork.setDuration((long) -1);

        testWorkRepo.saveAndFlush(testWork);
    }

    public TestWork getTestWork(String code){
        TestWork testWork = testWorkRepo.findByCode(code);
        if(!testWork.isOver())
        if(testWork.getEnd() < System.currentTimeMillis()) {
            testWork.setOver(true);
            testWorkRepo.saveAndFlush(testWork);
        }
        return testWork;
    }

    public List<TestWork> getActive(User user){
        return testWorkRepo.findAllByOwnerAndOver(user.getId(), false);
    }

    public List<TestWork> getAll(User user){
        return testWorkRepo.findAllByOwner(user.getId());
    }
}
