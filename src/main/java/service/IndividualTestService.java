package service;

import model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repo.*;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.*;

@Service
public class IndividualTestService {
    @Autowired
    private IndividualTestRepo indTestRepo;
    @Autowired
    private IndividualTestTaskRepo indTaskRepo;
    @Autowired
    private IndividualTestAnswerRepo indAnswerRepo;
    @Autowired
    private TestWorkRepo testWorkRepo;
    @Autowired
    private AnswerRepo answerRepo;
    @Autowired
    private TaskRepo taskRepo;


    private LinkedList<IndividualTestTask> addTest(IndividualTest test){
        indTestRepo.saveAndFlush(test);

        LinkedList<IndividualTestTask> tasks = generateTaskSet(test);
        LinkedList<IndividualTestTask> toReturn = new LinkedList<>();
        toReturn.addAll(tasks);
        return toReturn;
    }

    public LinkedList<IndividualTestTask> generateTaskSet(IndividualTest test){
        LinkedList<IndividualTestTask> individualTasks = new LinkedList<>();
        LinkedList<Task> tasks = new LinkedList<>();
        tasks.addAll(taskRepo.findAllByTest(test.getTestWork().getTest().getId()));
        tasks = selectRandom(tasks, test.getTestWork().getNumberOfQuestions());

        Iterator<Task> iterator = tasks.iterator();
        while (iterator.hasNext()){
            individualTasks.add(new IndividualTestTask());
            individualTasks.getLast().setTask(iterator.next());
            LinkedList <Answer> tmp = new LinkedList<>(answerRepo.findAllByTask(individualTasks.getLast().getTask().getId()));
            tmp = selectRandom( tmp, test.getTestWork().getAnsPerQuest());

            individualTasks.getLast().setAnswers(new LinkedList<IndividualTestAnswer>());
            for (int i = 0; i < tmp.size(); i++) {
                individualTasks.getLast().getAnswers().add(new IndividualTestAnswer());
                individualTasks
                        .getLast()
                        .getAnswers()
                        .get(individualTasks.getLast().getAnswers().size() - 1)
                        .setAnswer(tmp.get(i));
                indAnswerRepo.save(
                        individualTasks
                        .getLast()
                        .getAnswers()
                        .get(individualTasks.getLast().getAnswers().size() - 1));
            }
            indTaskRepo.save(individualTasks.getLast());
        }

        return individualTasks;
    }

    public <T>LinkedList<T> selectRandom(LinkedList<T> collection, int number) {
        Random rand = new Random();
        LinkedList<T> list = new LinkedList<>();
        list.addAll(collection);

        LinkedList<T> set = new LinkedList<>();
        for (int i = 0; i < collection.size() && i < number; i++) {
            set.add( list.get(rand.nextInt(list.size())));
        }
        set.addAll(list);
        return set;
    }

    public String updateIndTask(IndividualTestTask testTask){
        if(testTask.getIndividualTest().getTestWork().getOver() ||
           testTask.getIndividualTest().getTestWork().getEnd() < System.currentTimeMillis() &&
           testTask.getIndividualTest().getTestWork().getEnd() != -1 ||
           testTask.getIndividualTest().getBegin() + testTask.getIndividualTest().getTestWork().getDuration() > System.currentTimeMillis()
        ){

            testTask.getIndividualTest().getTestWork().setEnd(System.currentTimeMillis());
            testTask.getIndividualTest().getTestWork().setOver(true);
            testWorkRepo.saveAndFlush(testTask.getIndividualTest().getTestWork());

            finishTheTest(testTask.getIndividualTest());
            return "time is over";
        }

        indTaskRepo.saveAndFlush(testTask);

        return "success";
    }

    public void finishTheTest(IndividualTest test){
        test.setEnd(System.currentTimeMillis());
        test.setFinished(true);
        test.setScore( calculateScore(test));
        indTestRepo.saveAndFlush(test);
    }

    public int calculateScore(IndividualTest test){
        int score = 0;
        LinkedList<IndividualTestTask> tasks = new LinkedList<>( indTaskRepo.getAllByIndividualTest(test.getId()));
        Iterator<IndividualTestTask> individualTestTaskIterator= tasks.iterator();
        while (individualTestTaskIterator.hasNext()){
            LinkedList<IndividualTestAnswer> answers = new LinkedList<>(individualTestTaskIterator.next().getAnswers());
            int counter = 0;
            for (int i = 0; i < individualTestTaskIterator.next().getAnswers().size(); i++) {
                if(answers.get(i).getSelected() != answers.get(i).getAnswer().isCorrect())
                    break;
                counter++;
            }
            if(counter==answers.size())
                score++;
        }

        return score;
    }
}
