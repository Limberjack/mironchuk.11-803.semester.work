package service;

import model.Answer;
import model.Task;
import model.Test;
import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import repo.AnswerRepo;
import repo.TaskRepo;
import repo.TestRepo;

import java.util.*;

@Service
public class TestService {
    @Autowired
    private TestRepo testRepo;
    @Autowired
    private TaskRepo taskRepo;
    @Autowired
    private AnswerRepo answerRepo;

    private Test test;
    private List<Task> tasks;
    private List<Answer> answers;

    public void addTest(Test test){
        testRepo.saveAndFlush(test);
    }

    public void addTask(Test test, Task task, List<Answer> answers){
        task.setTest(test);
        taskRepo.save(task);
        Iterator<Answer> i = answers.iterator();
        while(i.hasNext())
            addAnswer(task, i.next());

        taskRepo.flush();
        answerRepo.flush();
    }

    public void addAnswer(Task task, Answer answer){
        answer.setTask(task);
        answerRepo.save(answer);
    }

    public List<Test> getTests(User user){
        return testRepo.getAllByOwner(user.getId());
    }

    public Map<String, Object> fillTheTest(Test test){
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("test", test);
        List<Task> tasks = taskRepo.findAllByTest(test.getId());
        map.put("tasks", tasks);
        LinkedList<List<Answer>> answers = new LinkedList<List<Answer>>();
        Iterator<Task> taskIterator = tasks.iterator();
        while (taskIterator.hasNext()){
            answers.add(answerRepo.findAllByTask(taskIterator.next().getId()));
        }
        map.put("answers", answers);

        return map;
    }
}
