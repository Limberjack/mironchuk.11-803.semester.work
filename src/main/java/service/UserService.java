package service;

import config.UserAuthentication;
import model.User;
import model.UserGoogle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import repo.UserGoogleRepo;
import repo.UserRepository;

import java.util.LinkedList;


@Service
public class UserService implements UserDetailsService{
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MailSender mailSender;
    @Autowired
    private UserGoogleRepo userGoogleRepo;
    private PasswordEncoder passwordEncoder;


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return userRepository.getByEmail(s);
    }

    public boolean addUser(User user){
        User someUser = userRepository.getByEmail(user.getEmail());
        if(someUser!= null){
            return false;
        }

        user.setPassword(getPasswordEncoder().encode(user.getPassword()));
        userRepository.saveAndFlush(user);
        return true;
    }

    public User getUser(String email, String password){
        User user = userRepository.getByEmail(email);
        if (user == null ||  passwordEncoder.matches(user.getPassword(), password)) {
            System.out.println("wrong email or password");
            return null;
        }

        UserAuthentication authentication = new UserAuthentication();
        authentication.setUser(user);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        SecurityContextHolder.getContext().getAuthentication().setAuthenticated(true);
        return user;
    }

    public UserGoogle getGoogleUser(String id){
        UserGoogle userGoogle = userGoogleRepo.getById(id);
        return userGoogle;
    }

    public PasswordEncoder getPasswordEncoder(){
        if(passwordEncoder == null)
            passwordEncoder = new BCryptPasswordEncoder(8);
        return passwordEncoder;
    }

    public MailSender getMailSender(){
        if(mailSender == null)
            mailSender = new MailSender();
        return mailSender;
    }

    public void saveGoogleUser(UserGoogle userGoogle){
        userGoogle.getUser().setPassword("google_user");
        userRepository.saveAndFlush(userGoogle.getUser());
        userGoogleRepo.saveAndFlush(userGoogle);
    }

}
